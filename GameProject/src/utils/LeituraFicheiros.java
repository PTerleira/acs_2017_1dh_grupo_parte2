/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import model.GameFunctions;
import model.Local;

/**
 *
 * @author pedroterleira
 */
public class LeituraFicheiros {

    private int contadorLinhasLocais;
    private int contadorLinhasCaminhos;
    private int contadorLinhasPersonagens;
    private int contadorLinhasAliancas;

    public void A1_leituraFicheiroLocaisCaminhos(String nomeFicheiro) {
        System.out.println("Conteúdo do arquivo Locais/Caminhos:");
        try {
            FileReader arq = new FileReader(nomeFicheiro);
            BufferedReader lerArq = new BufferedReader(arq);
            int flag = 0;
            String linha = lerArq.readLine();
            while (linha != null) {
                System.out.printf("%s\n", linha);
                if (linha.trim().equalsIgnoreCase("LOCAIS")) {
                    flag = 1;
                } else if (linha.trim().equalsIgnoreCase("CAMINHOS")) {
                    flag = 2;
                }
                if (flag == 1 && !linha.equalsIgnoreCase("LOCAIS")) {
                    String arrayLocal[] = new String[2];
                    arrayLocal = linha.trim().split(",");
                    Local local = new Local(arrayLocal[0], Integer.parseInt(arrayLocal[1]));
                    
                    
                    //Inserir vertice local no graph
                    if (GameFunctions.getInstance().A1_inserirLocal(local)) {
                        contadorLinhasLocais++;
                    }

                } else if (flag == 2 && !linha.equalsIgnoreCase("CAMINHOS")) {
                    String arrayCaminho[] = new String[3];
                    arrayCaminho = linha.trim().split(",");
                    //Inserir edge caminho no graph
                    if (GameFunctions.getInstance().A1_inserirCaminho(arrayCaminho[0], arrayCaminho[1], Double.parseDouble(arrayCaminho[2]))) {
                        contadorLinhasCaminhos++;
                    }
                }

                linha = lerArq.readLine();
            }

            arq.close();
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        System.out.println("\n");
    }

    public void A2_leituraFicheiroPersonagemAlianca(String nomeFicheiro) {
        System.out.println("Conteúdo do arquivo Personagens/Alianças:");
        try {
            FileReader arq = new FileReader(nomeFicheiro);
            BufferedReader lerArq = new BufferedReader(arq);
            int flag = 0;
            String linha = lerArq.readLine();
            while (linha != null) {
                System.out.printf("%s\n", linha);
                if (linha.trim().equalsIgnoreCase("PERSONAGENS")) {
                    flag = 1;
                } else if (linha.trim().equalsIgnoreCase("ALIANÇAS")) {
                    flag = 2;
                }
                if (flag == 1 && !linha.equalsIgnoreCase("PERSONAGENS")) {
                    String arrayPersonagem[] = new String[3];
                    arrayPersonagem = linha.trim().split(",");
                    //Inserir personagem como vertice do Graph
                    if (GameFunctions.getInstance().A2_inserirPersonagem(arrayPersonagem[0],Integer.parseInt(arrayPersonagem[1]),arrayPersonagem[2])) {
                        contadorLinhasPersonagens++;
                    }

                } else if (flag == 2 && !linha.equalsIgnoreCase("ALIANÇAS")) {
                    String arrayAlianca[] = new String[4];
                    arrayAlianca = linha.trim().split(",");
                    //Inserir alianças como edge no Graph
                    if (GameFunctions.getInstance().A2_D2_inserirAlianca(arrayAlianca[0], arrayAlianca[1], Boolean.parseBoolean(arrayAlianca[2]), Float.parseFloat(arrayAlianca[3]))) {
                        contadorLinhasAliancas++;
                    }
                }

                linha = lerArq.readLine();
            }

            arq.close();
        } catch (IOException e) {
            System.err.printf("Ficheiro não encontrado: %s.\n",
                    e.getMessage());
        }

        System.out.println("\n");
    }

    // Métodos criados para complementar os testes unitários
    public int getNumeroLinhasLocais() {
        return contadorLinhasLocais;
    }

    public int getNumeroLinhasCaminhos() {
        return contadorLinhasCaminhos;
    }

    public int getNumeroLinhasPersonagens() {
        return contadorLinhasPersonagens;
    }

    public int getNumeroLinhasAliancas() {
        return contadorLinhasAliancas;
    }
}
