/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author pedroterleira
 */
public class Personagem {
    
    private String nome;
    private int pontos;
    private LinkedList<Local> ownLocals;

    public Personagem(String nome, int pontos, Local l) {
        this.nome = nome;
        this.pontos = pontos;
        ownLocals=new LinkedList<>();
        ownLocals.add(l);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }
    
    public boolean addOwnLocals(Local local){
        return ownLocals.add(local);
    }

    public LinkedList<Local> getOwnLocals() {
        return ownLocals;
    }

    @Override
    public String toString() {
        return String.format("\nPersonagem\nNome: "+nome+"\nPontos: "+pontos+"\n");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personagem other = (Personagem) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return true;
    }
    

    
    
    
    
}
