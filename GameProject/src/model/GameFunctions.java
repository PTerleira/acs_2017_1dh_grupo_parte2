/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import graph.AdjacencyMatrixGraph;
import graph.EdgeAsDoubleGraphAlgorithms;
import graphbase.Edge;
import graphbase.Graph;
import java.util.LinkedList;

/**
 *
 * @author pedroterleira
 */
public class GameFunctions {

    private static GameFunctions instance;
    private static AdjacencyMatrixGraph<Local, Double> graphLocalCaminho;
    private static Graph<Personagem, Boolean> graphPersonagemAlianca;

    public static GameFunctions getInstance() {
        if (instance == null) {
            instance = new GameFunctions();
        }
        return instance;
    }

    public GameFunctions() {
        if (graphLocalCaminho == null) {
            graphLocalCaminho = new AdjacencyMatrixGraph<>();
        }
        if (graphPersonagemAlianca == null) {
            graphPersonagemAlianca = new Graph<>(false);
        }
    }

    public boolean A1_inserirLocal(Local l) {
        return graphLocalCaminho.insertVertex(l);
    }

    public boolean A1_inserirCaminho(String nomeLocal1, String nomeLocal2, Double dificuldade) {
        boolean flag = false;
        if (graphLocalCaminho.vertices() != null) {
            Local l1 = A1_searchLocalByName(nomeLocal1);
            Local l2 = A1_searchLocalByName(nomeLocal2);
            flag = graphLocalCaminho.insertEdge(l1, l2, dificuldade);
        } else {
            throw new NullPointerException("Locais passados como parametro não encontrados!");
        }
        return flag;
    }

    public Local A1_searchLocalByName(String nome) {
        if (graphLocalCaminho.vertices() != null) {
            for (Local l : graphLocalCaminho.vertices()) {
                if (nome.equalsIgnoreCase(l.getNome())) {
                    return l;
                }
            }
        }
        throw new NullPointerException("Local não encontrado!");
    }

    public double B1_caminhoComMenorDificuldade(Local localInicio, Local localFim, LinkedList<Local> path) {
        return EdgeAsDoubleGraphAlgorithms.shortestPath(graphLocalCaminho, localInicio, localFim, path);
    }

    public boolean C1_verificarSePersonagemPodeConquistarLocal(Personagem p, Local l, int[] forcaNecessaria, LinkedList<Local> path) {
        // Conhecer o menor caminho possivel para chegar ao local que pretende conquistar
        B1_caminhoComMenorDificuldade(p.getOwnLocals().getFirst(), l, path);
        // Por numa lista todas as personagens para saber se alguma personagem é dona de algum vertice passado
        LinkedList<Personagem> listaPersonagens = new LinkedList<Personagem>();
        for (Personagem personagem : graphPersonagemAlianca.vertices()) {
            listaPersonagens.add(personagem);
        }
        // Percorrer a lista de dois em dois locais para obter a dificuldade entre os dois
        Local localTemporario = path.getFirst();
        for (Local local : path) {
            // Verificação para a primeira posição não ser comparada com a primeira posição
            if (!local.equals(localTemporario)) {
                // Obter a dificuldade do caminho e somar à força necessária
                forcaNecessaria[0] += graphLocalCaminho.getEdge(localTemporario, local);
                // Obter a pontos do local e somar à força necessária
                forcaNecessaria[0] += local.getPontos();
                // Verificar se alguma personagem é dona do local
                for (Personagem personagem : listaPersonagens) {
                    
                    for (Local ownLocal : personagem.getOwnLocals()) {
                        if (ownLocal.equals(local)) {
                            // Se uma personagem for dona do local acresce à força necessária o numero de pontos dessa personagem
                            forcaNecessaria[0] += personagem.getPontos();
                            break;
                        }
                    }
                }
                localTemporario = local;
            }

        }
        return p.getPontos() > forcaNecessaria[0];
    }

    public boolean A2_inserirPersonagem(String nome, int pontos, String nomeLocalInicio) {
        Local local = A1_searchLocalByName(nomeLocalInicio);
        if (local != null) {
            Personagem personagem = new Personagem(nome, pontos, local);
            graphPersonagemAlianca.insertVertex(personagem);
            return true;
        }
        return false;
    }

    public boolean A2_D2_inserirAlianca(String nomePersonagem1, String nomePersonagem2, boolean acesso, float fatorCompatibilidade) {
        boolean flag = false;
        if (graphPersonagemAlianca.vertices() != null) {
            Personagem p1 = A2_searchPersonagemByName(nomePersonagem1);
            Personagem p2 = A2_searchPersonagemByName(nomePersonagem2);
            if (p1.equals(p2)) {
                throw new IllegalArgumentException("Alianças não podem ser feita entre a mesma personagem!");
            }
            // Verificar se aliança proposta a ser adicionada, já existe
            for (Edge<Personagem, Boolean> edge : graphPersonagemAlianca.edges()) {
                if ((edge.getVOrig().equals(p1) || edge.getVOrig().equals(p2)) && (edge.getVDest().equals(p1) || edge.getVDest().equals(p2))) {
                    throw new IllegalArgumentException("Aliança já criada!");
                }
            }
            flag = graphPersonagemAlianca.insertEdge(p1, p2, acesso, fatorCompatibilidade);
        }
        return flag;
    }

    public Personagem A2_searchPersonagemByName(String nome) {
        if (graphPersonagemAlianca.vertices() != null) {
            for (Personagem p : graphPersonagemAlianca.vertices()) {
                if (nome.equalsIgnoreCase(p.getNome())) {
                    return p;
                }
            }
        }
        throw new NullPointerException("Personagem não encontrada!");
    }

    public LinkedList<Personagem> B2_getListaAliados(Personagem p) {
        LinkedList<Personagem> listaAliados = new LinkedList<>();
        for (Personagem personagem : graphPersonagemAlianca.adjVertices(p)) {
            listaAliados.add(personagem);
        }
        return listaAliados;
    }

    public float C2_getAliancaMaisForte(Personagem arrayPersonagens[]) {
        float maiorForcaAlianca = 0.0f;
        float forcaAlianca = 0.0f;
        for (Edge<Personagem, Boolean> edge : graphPersonagemAlianca.edges()) {
            forcaAlianca = (float) ((edge.getVOrig().getPontos() + edge.getVDest().getPontos()) * edge.getWeight());
            if (forcaAlianca > maiorForcaAlianca) {
                maiorForcaAlianca = forcaAlianca;
                arrayPersonagens[0] = edge.getVOrig();
                arrayPersonagens[1] = edge.getVDest();
            }
        }
        return maiorForcaAlianca;
    }

    public Graph<Personagem, Boolean> E2_getGraphAliancasPossiveis() {
        Graph<Personagem, Boolean> graphAliancasPossiveis = new Graph<>(false);
        LinkedList<Personagem> listaAliados = new LinkedList<>();
        // As personagens têm que ser inseridas todas no inicio, no novo grafo para ficarem com a mesma ordem do grafo original
        for (Personagem p : graphPersonagemAlianca.vertices()) {
            graphAliancasPossiveis.insertVertex(p);
        }
        // Personagem a personagem verificar quais as personagens que ainda não são aliança
        for (Personagem personagem : graphPersonagemAlianca.vertices()) {
            listaAliados = B2_getListaAliados(personagem);
            // Se a personagem não tiver uma aliança com todas as outras personagens
            if (listaAliados.size() != graphPersonagemAlianca.numVertices() - 1) {
                
                int i = 0;
                for (Personagem p1 : graphPersonagemAlianca.vertices()) {
                    Personagem p2 = listaAliados.get(i);
                    if (!p1.equals(p2)) {
                        if (!p1.equals(personagem)) {
                            graphAliancasPossiveis.insertEdge(personagem, p1, true, 0.5f);
                        }
                    } else if (i < listaAliados.size() - 1) {
                        i++;
                    }
                }
            }
        }
        return graphAliancasPossiveis;
    }

    public AdjacencyMatrixGraph<Local, Double> getGraphLocalCaminho() {
        return graphLocalCaminho;
    }

    public Graph<Personagem, Boolean> getGraphPersonagemAlianca() {
        return graphPersonagemAlianca;
    }
}
