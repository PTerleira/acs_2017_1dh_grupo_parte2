package graph;

import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithms {

    /**
     * Determine the shortest path to all vertices from a vertex using
     * Dijkstra's algorithm To be called by public short method
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param knownVertices previously discovered vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param minDist minimum distances in the path
     *
     */
    private static <V> void shortestPath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, boolean[] knownVertices, int[] verticesIndex, double[] minDist) {
        int nv = graph.numVertices;

        for (int i = 0; i < nv; i++) {
            minDist[i] = Double.MAX_VALUE;
            verticesIndex[i] = -1;
            knownVertices[i] = false;
        }

        minDist[sourceIdx] = 0;
        while (sourceIdx != -1) {
            knownVertices[sourceIdx] = true;

            for (int i = 0; i < nv; i++) {
                if (graph.privateGet(sourceIdx, i) != null) {
                    if (!knownVertices[i] && minDist[i] > minDist[sourceIdx] + graph.privateGet(sourceIdx, i)) {
                        minDist[i] = minDist[sourceIdx] + graph.privateGet(sourceIdx, i);
                        verticesIndex[i] = sourceIdx;
                    }
                }
            }

            // Proximo sourceIdx -> procura o vertice com menor distância e que não foi visitado;
            sourceIdx = -1;
            double min = Double.MAX_VALUE;

            for (int i = 0; i < nv; i++) {
                if (knownVertices[i] == false && minDist[i] < min) {
                    min = minDist[i];
                    sourceIdx = i;
                }
            }
        }
    }

    /**
     * Determine the shortest path between two vertices using Dijkstra's
     * algorithm
     *
     * @param graph Graph object
     * @param source Source vertex
     * @param dest Destination vertices
     * @param path Returns the vertices in the path (empty if no path)
     * @return minimum distance, -1 if vertices not in graph or no path
     *
     */
    public static <V> double shortestPath(AdjacencyMatrixGraph<V, Double> graph, V source, V dest, LinkedList<V> path) {
        int destIndex = graph.toIndex(dest);
        int sourceIdx = graph.toIndex(source);

        if (destIndex == -1 || sourceIdx == -1) {
            return -1;
        }

        path.clear();

        int nv = graph.numVertices;

        boolean[] knownVertices = new boolean[nv];
        int[] verticesIndex = new int[nv];
        double[] minDist = new double[nv];

        shortestPath(graph, sourceIdx, knownVertices, verticesIndex, minDist);

        if (knownVertices[destIndex] == false) {
            return -1;
        }

        recreatePath(graph, sourceIdx, destIndex, verticesIndex, path);

        LinkedList<V> stack = new LinkedList<>();  //create a stack

        while (!path.isEmpty()) {
            stack.push(path.remove());
        }

        while (!stack.isEmpty()) {
            path.add(stack.pop());
        }

        return minDist[destIndex];
    }

    /**
     * Recreates the minimum path between two vertex, from the result of
     * Dikstra's algorithm
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param destIdx Destination vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param Queue Vertices in the path (empty if no path)
     */
    private static <V> void recreatePath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, int destIdx, int[] verticesIndex, LinkedList<V> path) {

        path.add(graph.vertices.get(destIdx));
        if (sourceIdx != destIdx) {
            destIdx = verticesIndex[destIdx];
            recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        }
    }

    /**
     * Creates new graph with minimum distances between all pairs uses the
     * Floyd-Warshall algorithm
     *
     * @param graph Graph object
     * @return the new graph
     */
    public static <V> AdjacencyMatrixGraph<V, Double> minDistGraph(AdjacencyMatrixGraph<V, Double> graph) {

        AdjacencyMatrixGraph<V, Double> minDist = (AdjacencyMatrixGraph<V, Double>) graph.clone();

        int nv = minDist.numVertices;

        for (int k = 0; k < nv; k++) {
            for (int i = 0; i < nv; i++) {
                if (i != k && minDist.privateGet(i, k) != null) {
                    for (int j = 0; j < nv; j++) {
                        if (i != j && j != k && minDist.privateGet(k, j) != null) {
                            if (minDist.privateGet(i, j) == null) {
                                minDist.insertEdge(i, j, minDist.privateGet(i, k) + minDist.privateGet(k, j));
                            } else if (minDist.privateGet(i, j) > minDist.privateGet(i, k) + minDist.privateGet(k, j)) {
                                minDist.privateSet(i, j, minDist.privateGet(i, k) + minDist.privateGet(k, j));
                            }
                        }
                    }
                }
            }
        }

        return minDist;
    }

}
