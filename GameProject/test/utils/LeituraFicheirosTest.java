/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import model.GameFunctions;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedroterleira
 */
public class LeituraFicheirosTest {

    /**
     * Test of A1_leituraFicheiroLocaisCaminhos method, of class
     * LeituraFicheiros.
     */
    @Test
    public void testA1_leituraFicheiroLocaisCaminhos() {
        System.out.println("A1_leituraFicheiroLocaisCaminhos");
        LeituraFicheiros instance = new LeituraFicheiros();
        instance.A1_leituraFicheiroLocaisCaminhos("inputTestLocais.txt");

        assertEquals(instance.getNumeroLinhasLocais(), GameFunctions.getInstance().getGraphLocalCaminho().numVertices());
        assertEquals(instance.getNumeroLinhasCaminhos(), GameFunctions.getInstance().getGraphLocalCaminho().numEdges());
    }

    /**
     * Test of A2_leituraFicheiroPersonagemAlianca method, of class
     * LeituraFicheiros.
     */
    @Test
    public void testA2_leituraFicheiroPersonagemAlianca() {
        System.out.println("A2_leituraFicheiroPersonagemAlianca");
        LeituraFicheiros instance = new LeituraFicheiros();
        instance.A2_leituraFicheiroPersonagemAlianca("inputTestPers.txt");

        assertEquals(instance.getNumeroLinhasPersonagens(), GameFunctions.getInstance().getGraphPersonagemAlianca().numVertices());
        // Segundo o que vi no debug, o adjency map guarda p.ex Local 1 com Local 2 e Local 2 com Local 1, quando é inserido edge 1,2?
        assertEquals(instance.getNumeroLinhasAliancas()*2, GameFunctions.getInstance().getGraphPersonagemAlianca().numEdges());
    }
}
