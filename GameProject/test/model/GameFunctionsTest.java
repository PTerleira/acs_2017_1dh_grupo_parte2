/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import graphbase.Edge;
import graphbase.Graph;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.LeituraFicheiros;

/**
 *
 * @author pedroterleira
 */
public class GameFunctionsTest {

    /**
     * Test of A1_inserirLocal method, of class GameFunctions.
     */
    @Test
    public void testA1_inserirLocal() {
        System.out.println("A1_inserirLocal");

        Local l = new Local("LocalTeste", 100);
        GameFunctions instance = new GameFunctions();

        int numeroLocais = GameFunctions.getInstance().getGraphLocalCaminho().numVertices();
        boolean result = instance.A1_inserirLocal(l);

        assertTrue("Resultado de inserir local deve ser true", result == true);
        assertEquals(numeroLocais + 1, GameFunctions.getInstance().getGraphLocalCaminho().numVertices());
        assertEquals(GameFunctions.getInstance().A1_searchLocalByName("LocalTeste"), l);
    }

    /**
     * Test of A1_inserirCaminho method, of class GameFunctions.
     */
    @Test
    public void testA1_inserirCaminho() {
        System.out.println("A1_inserirCaminho");

        GameFunctions instance = new GameFunctions();
        int numeroCaminhos = instance.getGraphLocalCaminho().numEdges();
        boolean result = instance.A1_inserirCaminho("Local3", "Local9", 20.00);

        assertTrue("Resultado de caminho deve ser true", result == true);
        assertEquals(numeroCaminhos + 1, GameFunctions.getInstance().getGraphLocalCaminho().numEdges());
    }

    /**
     * Test of B1_caminhoComMenorDificuldade method, of class GameFunctions.
     */
    @Test
    public void testB1_caminhoComMenorDificuldade() {
        System.out.println("B1_caminhoComMenorDificuldade");

        GameFunctions instance = new GameFunctions();

        LinkedList<Local> path = new LinkedList<Local>();

        assertTrue("Resultado deve ser 0 se os vertices são iguais", instance.B1_caminhoComMenorDificuldade(instance.A1_searchLocalByName("Local1"), instance.A1_searchLocalByName("Local1"), path) == 0);

        assertTrue("O tamanho da lista path deve ser 1 se o vertice de origem e o vertice forem iguais", path.size() == 1);

        assertTrue("Dificuldade entre o Local1 o Local6 deve ser 45", instance.B1_caminhoComMenorDificuldade(instance.A1_searchLocalByName("Local1"), instance.A1_searchLocalByName("Local6"), path) == 45);
        Iterator<Local> it = path.iterator();
        it = path.iterator();

        assertTrue("Primeiro na lista path deve ser o Local1", it.next().equals(instance.A1_searchLocalByName("Local1")) == true);
        assertTrue("depois Local2", it.next().equals(instance.A1_searchLocalByName("Local2")) == true);
        assertTrue("depois Local6", it.next().equals(instance.A1_searchLocalByName("Local6")) == true);

        assertTrue("Dificuldade entre o Local1 o Local10 deve ser 50", instance.B1_caminhoComMenorDificuldade(instance.A1_searchLocalByName("Local1"), instance.A1_searchLocalByName("Local10"), path) == 50);
        it = path.iterator();

        assertTrue("Primeiro na lista path deve ser o Local1", it.next().equals(instance.A1_searchLocalByName("Local1")) == true);
        assertTrue("depois Local2", it.next().equals(instance.A1_searchLocalByName("Local2")) == true);
        assertTrue("depois Local8", it.next().equals(instance.A1_searchLocalByName("Local8")) == true);
        assertTrue("depois Local10", it.next().equals(instance.A1_searchLocalByName("Local10")) == true);

    }

    /**
     * Test of A1_searchLocalByName method, of class GameFunctions.
     */
    @Test
    public void testA1_searchLocalByName() {
        System.out.println("A1_searchLocalByName");

        LeituraFicheiros leituraInstance = new LeituraFicheiros();
        leituraInstance.A1_leituraFicheiroLocaisCaminhos("inputTestLocais.txt");
        GameFunctions instance = new GameFunctions();

        Local result = instance.A1_searchLocalByName("Local5");
        Local expResult = new Local("Local5", 35);
        assertEquals(expResult, result);
    }

    /**
     * Test of A2_searchPersonagemByName method, of class GameFunctions.
     */
    @Test
    public void testA2_searchPersonagemByName() {
        System.out.println("A2_searchPersonagemByName");

        GameFunctions instance = new GameFunctions();

        Personagem expResult = new Personagem("Pers5", 262, GameFunctions.getInstance().A1_searchLocalByName("Local5"));
        Personagem result = instance.A2_searchPersonagemByName("Pers5");
        assertEquals(expResult, result);
    }

    /**
     * Test of C1_verificarSePersonagemPodeConquistarLocal method, of class
     * GameFunctions.
     */
    @Test
    public void testC1_verificarSePersonagemPodeConquistarLocal() {
        System.out.println("C1_verificarSePersonagemPodeConquistarLocal");
        
        GameFunctions instance = new GameFunctions();
        int[] forcaNecessaria={0};
        Personagem p = GameFunctions.getInstance().A2_searchPersonagemByName("Pers4");
        LinkedList<Local> path = new LinkedList<>();
        boolean result = instance.C1_verificarSePersonagemPodeConquistarLocal(p,
                GameFunctions.getInstance().A1_searchLocalByName("Local5"),
                forcaNecessaria,
                path);

        Iterator<Local> it = path.iterator();
        it = path.iterator();

        assertTrue("Personagem deve poder conquistar o lugar, resultado deve ser true", result == true);
        assertTrue("Primeiro na lista path deve ser o Local9", it.next().equals(instance.A1_searchLocalByName("Local9")) == true);
        assertTrue("depois Local11", it.next().equals(instance.A1_searchLocalByName("Local11")) == true);
        assertTrue("depois Local7", it.next().equals(instance.A1_searchLocalByName("Local7")) == true);
        assertTrue("depois Local5", it.next().equals(instance.A1_searchLocalByName("Local5")) == true);
 
        assertEquals(357, forcaNecessaria[0]);
    }

    /**
     * Test of A2_inserirPersonagem method, of class GameFunctions.
     */
    @Test
    public void testA2_inserirPersonagem() {
        System.out.println("A2_inserirPersonagem");

        GameFunctions instance = new GameFunctions();
        int numeroPersonagens = instance.getGraphPersonagemAlianca().numVertices();
        boolean result = instance.A2_inserirPersonagem("Pers7", 150, "Local8");

        assertTrue("Resultado de inserir personagem deve ser true", result == true);
        assertEquals(numeroPersonagens + 1, GameFunctions.getInstance().getGraphPersonagemAlianca().numVertices());
    }

    /**
     * Test of A2_inserirAlianca method, of class GameFunctions.
     */
    @Test
    public void testA2_D2_inserirAlianca() {
        System.out.println("A2_inserirAlianca");

        GameFunctions instance = new GameFunctions();

        boolean result = instance.A2_D2_inserirAlianca("Pers2", "Pers3", true, 0.2f);

        assertTrue("Resultado de inserir aliança deve ser true", result == true);
        assertEquals(instance.getGraphPersonagemAlianca().numEdges(), GameFunctions.getInstance().getGraphPersonagemAlianca().numEdges());
    }

    /**
     * Test of B2_getListaAliados method, of class GameFunctions.
     */
    @Test
    public void B2_getListaAliados() {
        System.out.println("B2_getListaAliados");

        GameFunctions instance = new GameFunctions();

        LinkedList<Personagem> listaAliados = new LinkedList<>();
        Personagem p1 = GameFunctions.getInstance().A2_searchPersonagemByName("Pers1");
        listaAliados = instance.B2_getListaAliados(p1);

        assertEquals(3, listaAliados.size());

        int cont = 0;
        for (Personagem pers : listaAliados) {
            if (pers.getNome().equalsIgnoreCase("Pers3") || pers.getNome().equalsIgnoreCase("Pers4") || pers.getNome().equalsIgnoreCase("Pers2")) {
                cont++;
            }
        }

        assertEquals(3, cont);

        Personagem p2 = GameFunctions.getInstance().A2_searchPersonagemByName("Pers5");
        listaAliados = instance.B2_getListaAliados(p2);

        assertEquals(2, listaAliados.size());

        cont = 0;
        for (Personagem pers : listaAliados) {
            if (pers.getNome().equalsIgnoreCase("Pers3") || pers.getNome().equalsIgnoreCase("Pers6")) {
                cont++;
            }
        }
        
        assertEquals(2, cont);
    }

    /**
     * Test of C2_getAliancaMaisForte method, of class GameFunctions.
     */
    @Test
    public void C2_getAliancaMaisForte() {
        System.out.println("C2_getAliancaMaisForte");

        GameFunctions instance = new GameFunctions();
        LeituraFicheiros leituraInstance = new LeituraFicheiros();
        leituraInstance.A1_leituraFicheiroLocaisCaminhos("inputTestLocais.txt");
        leituraInstance.A2_leituraFicheiroPersonagemAlianca("inputTestPers.txt");

        Personagem personagensAlianca[] = new Personagem[2];
        float forca = instance.C2_getAliancaMaisForte(personagensAlianca);

        assertTrue("Forca should be 310,4", forca == 310.4f);

        int cont = 0;
        for (int i = 0; i < 2; i++) {
            if (personagensAlianca[i].equals(GameFunctions.getInstance().A2_searchPersonagemByName("Pers5")) || personagensAlianca[i].equals(GameFunctions.getInstance().A2_searchPersonagemByName("Pers6"))) {
                cont++;
            }

        }
        assertEquals(2, cont);
    }
    
        /**
     * Test of E2_getGraphAliancasPossiveis method, of class GameFunctions.
     */
    @Test
    public void testE2_getGraphAliancasPossiveis() {
        System.out.println("E2_getGraphAliancasPossiveis");

        GameFunctions instance = new GameFunctions();
        
        ArrayList<Integer> numeroAliados= new ArrayList<>();
        numeroAliados.add(2); // Para o Local1 o numero de alianças que podem ser feitas é 2
        numeroAliados.add(4); // Para o Local2 o numero de alianças que podem ser feitas é 4
        numeroAliados.add(2); // Para o Local3 o numero de alianças que podem ser feitas é 2
        numeroAliados.add(3); // Para o Local4 o numero de alianças que podem ser feitas é 3
        numeroAliados.add(3); // Para o Local5 o numero de alianças que podem ser feitas é 3
        numeroAliados.add(4); // Para o Local6 o numero de alianças que podem ser feitas é 4
        
        Graph<Personagem, Boolean> graphAliancasPossiveis = new Graph<>(false);
        graphAliancasPossiveis = instance.E2_getGraphAliancasPossiveis();
        
        int cont=0, i=0;
        for (Personagem p : graphAliancasPossiveis.vertices()) {
            for (Personagem edge : graphAliancasPossiveis.adjVertices(p)) {
                cont++;
            }
            assertEquals((Integer)numeroAliados.get(i),(Integer)cont);
            i++;
            cont=0;
        }
    }

}
